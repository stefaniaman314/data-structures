#include <iostream>
#include <fstream>
#include <queue>
#define CLEAR system("cls")
#define PAUSE system("pause")

using namespace std;

struct Node
{
	int key;
	Node* parent;
	Node* left;
	Node* right;

	Node(int k = 0)
	{
		key = k;
		parent = left = right = nullptr;
	}
};

struct SearchTree
{
	Node* root;

	SearchTree()
	{
		root = nullptr;
	}

	void Insert(Node* z, int k)
	{
		z->key = k;
		z->left = z->right = nullptr;

		Node* y = nullptr;
		Node* x = root;

		while (x != nullptr)
		{
			y = x;

			if (z->key < x->key)
				x = x->left;
			else
				x = x->right;
		}

		z->parent = y;

		if (y == nullptr)
			root = z;
		else
		{
			if (z->key < y->key)
				y->left = z;
			else
				y->right = z;
		}
	}

	Node* Maxim(Node* x)
	{
		Node* y = x;

		while (y->right != nullptr)
			y = y->right;

		return y;
	}

	Node* Minim(Node* x)
	{
		Node* y = x;

		while (y->left != nullptr)
			y = y->left;

		return y;
	}

	Node* Succesor(Node* x)
	{
		Node* y = nullptr;

		if (x->right != nullptr)
		{
			y = Minim(x->right);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->right)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Predecesor(Node* x)
	{
		Node* y = nullptr;

		if (x->left != nullptr)
		{
			y = Maxim(x->left);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->left)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Search(int k)
	{
		Node* x = root;

		while (x != nullptr && x->key != k)
		{
			if (k < x->key)
				x = x->left;
			else
				x = x->right;
		}

		return x;
	}
	
	void Transplant(Node* u, Node* v)
	{
		if (u->parent == nullptr)
			root = v;
		else
		{
			if (u == u->parent->left)
				u->parent->left = v;
			else
				u->parent->right = v;
		}

		if (v != nullptr)
			v->parent = u->parent;
	}

	void Delete(Node* z)
	{
		if (z->left == nullptr)
			Transplant(z, z->right);
		else
		{
			if (z->right == nullptr)
				Transplant(z, z->left);
			else
			{
				Node* y = Succesor(z);

				if (y != z->right)
				{
					Transplant(y, y->right);
					y->right = z->right;
					z->right->parent = y;
				}

				Transplant(z, y);
				y->left = z->left;
				z->left->parent = y;
			}
		}
	}

	void PrintPreorder(Node* node)
	{
		if (node == nullptr)
			return;

		cout << node->key << " ";
		PrintPreorder(node->left);
		PrintPreorder(node->right);
	}

	void PrintInorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintInorder(node->left);
		cout << node->key << " ";
		PrintInorder(node->right);
	}

	void PrintPostorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintPostorder(node->left);
		PrintPostorder(node->right);
		cout << node->key << " ";
	}

	void PrintLevelOrder()
	{
		if (root == nullptr)
			return;

		queue<Node*> q;
		q.push(root);

		while (q.empty() == false)
		{
			Node* node = q.front();
			q.pop();

			cout << node->key << " ";

			if (node->left != nullptr)
				q.push(node->left);

			if (node->right != nullptr)
				q.push(node->right);
		}
	}

	void PrintTree(int opt)
	{
		switch (opt)
		{

		case 0:
			return;

		case 1:
			cout << "Preorder: ";
			PrintPreorder(root);
			cout << '\n';
			break;

		case 2:
			cout << "Inorder: ";
			PrintInorder(root);
			cout << '\n';
			break;

		case 3:
			cout << "Postorder: ";
			PrintPostorder(root);
			cout << '\n';
			break;

		case 4:
			cout << "LevelOrder: ";
			PrintLevelOrder();
			cout << '\n';
			break;

		default:
			break;
		}
	}

	void Construct(int* keys, int size)
	{
		for (int i = 0; i < size; i++)
		{
			Node* x = new Node(keys[i]);
			Insert(x, x->key);
		}

		cout << "Constructed!\n";

	}

	void Empty()
	{
		if (root == nullptr)
			return;

		while (root != nullptr)
			delete root;

		cout << "Empty!\n";
	}
};

int main()
{
	SearchTree tree;

	int keys[5] = { 2, 4, 3, 1, 5 };

	tree.Construct(keys, 5);
	
	while (1)
	{
		//Show menu
		std::cout << "0. EXIT \n";
		std::cout << "1. INSERT \n";
		std::cout << "2. SEARCH \n";
		std::cout << "3. DELETE \n";
		std::cout << "4. MINIM \n";
		std::cout << "5. MAXIM \n";
		std::cout << "6. SUCCESOR \n";
		std::cout << "7. PREDECESOR \n";
		std::cout << "8. PRINT_TREE \n";
		std::cout << "9. EMPTY \n";

		//Get choice
		int choice;
		std::cout << "choice:";
		std::cin >> choice;

		//Options
		switch (choice)
		{
		case 0:
			return 0;

		case 1:
		{
			CLEAR;

			Node* node = new Node;

			int key;
			cout << "key = ";
			cin >> key;

			tree.Insert(node, key);

			cout << "SearchTree after insertion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 2:
		{	CLEAR;
			int key;
			std::cout << "key = ";
			std::cin >> key;

			Node* node = tree.Search(key);

			if (node != nullptr)
				cout << "Nodul " << node->key << " a fost gasit!\n";
			else
				cout << "Nodul " << node->key << " nu a fost gasit!\n";

			PAUSE;
			CLEAR;
		}break;

		case 3:
		{
			CLEAR;
			int key;
			std::cout << "key = ";
			std::cin >> key;

			Node* node = tree.Search(key);

			if (node != nullptr)
				tree.Delete(node);

			cout << "SearchTree after deletion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 4:
		{
			CLEAR;
			Node* node = tree.Minim(tree.root);

			cout << "Minim = " << node->key << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 5:
		{
			CLEAR;
			cout << "Maxim = " << tree.Maxim(tree.root)->key << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 6:
		{
			CLEAR;
			cout << "Succesor = " << tree.Succesor(tree.root)->key << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 7:
		{
			CLEAR;

			Node* node = tree.Search(3);
			cout << "Predecesor = " << tree.Predecesor(node)->key << '\n';

			PAUSE;
			CLEAR;
		}break;

 		case 8:
		{
			CLEAR;

			cout << "0. Exit Print \n";
			cout << "1. Print Preorder \n";
			cout << "2. Print Inorder \n";
			cout << "3. Print Postorder \n";
			cout << "4. Print LevelOrder \n";

			while(1)
			{
				int opt;
				cout << "option = ";
				cin >> opt;

				switch (opt)
				{

				case 0:
					return 0;

				case 1:
					tree.PrintTree(1);
					break;

				case 2:
					tree.PrintTree(2);
					break;

				case 3:
					tree.PrintTree(3);
					break;

				case 4:
					tree.PrintTree(4);
					break;

				default:
					break;
				}
			}
			PAUSE;
			CLEAR;
		}break;

		case 9:
		{
			CLEAR;

			tree.Empty();

			PAUSE;
			CLEAR;
		}break;

		}
	}
	
	system("pause");

	return 0;
}