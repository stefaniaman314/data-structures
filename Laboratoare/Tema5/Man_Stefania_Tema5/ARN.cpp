#include <iostream>
#include <fstream>
#include <queue>
#define CLEAR system("cls")
#define PAUSE system("pause")

using namespace std;

enum class Culoare
{
	Rosu = 0,
	Negru = 1
};

struct Node
{
	int info;
	int color;
	Node* parent;
	Node* left;
	Node* right;

	Node(int info = 0, int color = static_cast<int>(Culoare::Rosu))
	{
		this->info = info;
		this->color = color;

		parent = left = right = nullptr;
	}
};

struct ARN
{
	Node* root;
	Node* nil;

	ARN()
	{
		root = nil = nullptr;
	}

	Node* Maxim(Node* x)
	{
		Node* y = x;

		while (y->right != nullptr)
			y = y->right;

		return y;
	}

	Node* Minim(Node* x)
	{
		Node* y = x;

		while (y->left != nullptr)
			y = y->left;

		return y;
	}

	Node* Succesor(Node* x)
	{
		Node* y = nullptr;

		if (x->right != nullptr)
		{
			y = Minim(x->right);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->right)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Predecesor(Node* x)
	{
		Node* y = nullptr;

		if (x->left != nullptr)
		{
			y = Maxim(x->left);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->left)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Search(int k)
	{
		Node* x = root;

		while (x != nullptr && x->info != k)
		{
			if (k < x->info)
				x = x->left;
			else
				x = x->right;
		}

		return x;
	}

	void Rot_St(Node* x)
	{
		Node* y = x->right;
		x->right = y->left;

		if (y->left != nil)
			y->left->parent = x;

		y->parent = x->parent;

		if (x->parent == nil)
			root = y;
		else if (x == x->parent->left)
			x->parent->left = y;
		else
			x->parent->right = y;

		y->left = x;
		x->parent = y;
	}

	void Rot_Dr(Node* x)
	{
		Node* y = x->left;
		x->left = y->right;

		if (y->right != nil)
			y->right->parent = x;

		y->parent = x->parent;

		if (x->parent == nil)
			root = y;
		else if (x == x->parent->right)
			x->parent->right = y;
		else
			x->parent->left = y;

		y->right = x;
		x->parent = y;
	}

	void Insert_Repara(Node* z)
	{
		while (z->parent->color = static_cast<int>(Culoare::Rosu))
		{
			if (z->parent == z->parent->parent->left)
			{
				Node* uncle = z->parent->parent->right;

				if (uncle->color == static_cast<int>(Culoare::Rosu))
				{
					z->parent->color = static_cast<int>(Culoare::Negru);
					uncle->color = static_cast<int>(Culoare::Negru);
					z->parent->parent->color = static_cast<int>(Culoare::Rosu);

					z = z->parent->parent;
				}

				else
				{
					if (z = z->parent->right)
					{
						z = z->parent;

						Rot_St(z);
					}

					z->parent->color = static_cast<int>(Culoare::Negru);
					z->parent->parent->color = static_cast<int>(Culoare::Rosu);

					Rot_Dr(z->parent->parent);
				}
			}

			else
			{
				Node* uncle = z->parent->parent->left;

				if (uncle->color == static_cast<int>(Culoare::Rosu))
				{
					z->parent->color = static_cast<int>(Culoare::Negru);
					uncle->color = static_cast<int>(Culoare::Negru);
					z->parent->parent->color = static_cast<int>(Culoare::Rosu);

					z = z->parent->parent;
				}

				else
				{
					if (z = z->parent->left)
					{
						z = z->parent;

						Rot_Dr(z);
					}

					z->parent->color = static_cast<int>(Culoare::Negru);
					z->parent->parent->color = static_cast<int>(Culoare::Rosu);

					Rot_St(z->parent->parent);
				}
			}
			
		}
	}
	void Insert(Node* z, int info)
	{
		z->info = info;

		Node* y = nil;
		Node* x = root;
		
		while (x != nil)
		{
			y = x;

			if (z->info < x->info)
				x = x->left;
			else
				x = x->right;
		}

		z->parent = y;

		if (y == nil)
			root = z;
		else
		{
			if (z->info < y->info)
				y->left = z;
			else
				y->right = z;
		}

		z->left = nil;
		z->right = nil;
		z->color = static_cast<int>(Culoare::Rosu);

		Insert_Repara(z);
	}

	void Transplant(Node* u, Node* v)
	{
		if (u->parent == nil)
			root = v;
		else if (u == u->parent->left)
			u->parent->left = v;
		else
			u->parent->right = v;

		v->parent = u->parent;
	}

	void Delete_Repara(Node* x)
	{
		while (x != root && x->color == static_cast<int>(Culoare::Negru))
		{
			if (x == x->parent->left)
			{
				Node* w = x->parent->right;

				if (w->color == static_cast<int>(Culoare::Rosu))
				{
					w->color = static_cast<int>(Culoare::Negru);
					x->parent->color = static_cast<int>(Culoare::Rosu);
					Rot_St(x->parent);
					w = x->parent->right;
				}

				if (w->left->color == static_cast<int>(Culoare::Negru) && w->right->color == static_cast<int>(Culoare::Negru))
				{
					w->color = static_cast<int>(Culoare::Rosu);
					x = x->parent;
				}
				else if (w->right->color == static_cast<int>(Culoare::Negru))
				{
					w->left->color = static_cast<int>(Culoare::Negru);
					w->color = static_cast<int>(Culoare::Rosu);
					Rot_Dr(w);
					w = x->parent->right;
				}

				w->color = x->parent->color;
				x->parent->color = static_cast<int>(Culoare::Negru);
				Rot_St(x->parent);
				x = root;
			}

			else
			{
				Node* w = x->parent->left;

				if (w->color == static_cast<int>(Culoare::Rosu))
				{
					w->color = static_cast<int>(Culoare::Negru);
					x->parent->color = static_cast<int>(Culoare::Rosu);
					Rot_Dr(x->parent);
					w = x->parent->left;
				}

				if (w->right->color == static_cast<int>(Culoare::Negru) && w->left->color == static_cast<int>(Culoare::Negru))
				{
					w->color = static_cast<int>(Culoare::Rosu);
					x = x->parent;
				}
				else if (w->left->color == static_cast<int>(Culoare::Negru))
				{
					w->right->color = static_cast<int>(Culoare::Negru);
					w->color = static_cast<int>(Culoare::Rosu);
					Rot_St(w);
					w = x->parent->left;
				}

				w->color = x->parent->color;
				x->parent->color = static_cast<int>(Culoare::Negru);
				Rot_Dr(x->parent);
				x = root;
			}
		}

		x->color = static_cast<int>(Culoare::Negru);
	}

	void Delete(Node* z)
	{

		Node* x = nullptr;
		Node* y = z;
		int yColor = y->color;

		if (z->left == nil)
		{
			x = z->right;
			Transplant(z, z->right);
		}

		else if (z->right == nil)
		{
			x = z->left;
			Transplant(z, z->left);
		}

		else
		{
			y = Minim(z->right);
			yColor = y->color;
			x = y->right;

			if (y->parent == z)
				x->parent = y;
			else
			{
				Transplant(y, y->right);
				y->right = z->right;
				y->right->parent = y;
			}

			Transplant(z, y);
			y->left = z->left;
			y->left->parent = y;
			y->color = z->color;
		}

		if (yColor == static_cast<int>(Culoare::Negru))
			Delete_Repara(x);
	}

	void Construct(int* keys, int size)
	{
		for (int i = 0; i < size; i++)
		{
			Node* x = new Node;
			x->info = keys[i];
			Insert(x, x->info);
		}

		cout << "Constructed!\n";
	}

	void Empty()
	{
		if (root == nullptr)
			return;

		while (root != nullptr)
			delete root;

		cout << "Empty!\n";
	}


	void PrintPreorder(Node* node)
	{
		if (node == nullptr)
			return;

		cout << node->info << " ";
		PrintPreorder(node->left);
		PrintPreorder(node->right);
	}

	void PrintInorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintInorder(node->left);
		cout << node->info << " ";
		PrintInorder(node->right);
	}

	void PrintPostorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintPostorder(node->left);
		PrintPostorder(node->right);
		cout << node->info << " ";
	}

	void PrintLevelOrder()
	{
		if (root == nullptr)
			return;

		queue<Node*> q;
		q.push(root);

		while (q.empty() == false)
		{
			Node* node = q.front();
			q.pop();

			cout << node->info << " ";

			if (node->left != nullptr)
				q.push(node->left);

			if (node->right != nullptr)
				q.push(node->right);
		}
	}

	void PrintTree(int opt)
	{
		switch (opt)
		{

		case 0:
			return;

		case 1:
			cout << "Preorder: ";
			PrintPreorder(root);
			cout << '\n';
			break;

		case 2:
			cout << "Inorder: ";
			PrintInorder(root);
			cout << '\n';
			break;

		case 3:
			cout << "Postorder: ";
			PrintPostorder(root);
			cout << '\n';
			break;

		case 4:
			cout << "LevelOrder: ";
			PrintLevelOrder();
			cout << '\n';
			break;

		default:
			break;
		}
	}
};

int main()
{
	ARN tree;

	int keys[5] = { 2, 4, 3, 1, 5 };

	tree.Construct(keys, 5);

	while (1)
	{
		//Show menu
		std::cout << "0. EXIT \n";
		std::cout << "1. INSERT \n";
		std::cout << "2. SEARCH \n";
		std::cout << "3. DELETE \n";
		std::cout << "4. MINIM \n";
		std::cout << "5. MAXIM \n";
		std::cout << "6. SUCCESOR \n";
		std::cout << "7. PREDECESOR \n";
		std::cout << "8. PRINT_TREE \n";
		std::cout << "9. EMPTY \n";

		//Get choice
		int choice;
		std::cout << "choice:";
		std::cin >> choice;

		//Options
		switch (choice)
		{
		case 0:
			return 0;

		case 1:
		{
			CLEAR;

			Node* node = new Node;

			int key;
			cout << "key = ";
			cin >> key;

			tree.Insert(node, key);

			cout << "ARN after insertion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 2:
		{	CLEAR;
		
			int key;
			std::cout << "key = ";
			std::cin >> key;

			Node* node = tree.Search(key);

			if (node != nullptr)
				cout << "Nodul " << node->info << " a fost gasit!\n";
			else
				cout << "Nodul " << node->info << " nu a fost gasit!\n";

			PAUSE;
			CLEAR;
		}break;

		case 3:
		{
			CLEAR;

			int key;
			std::cout << "key = ";
			std::cin >> key;

			Node* node = tree.Search(key);

			if (node != nullptr)
				tree.Delete(node);

			cout << "ARN after deletion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 4:
		{
			CLEAR;
			Node* node = tree.Minim(tree.root);

			cout << "Minim = " << node->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 5:
		{
			CLEAR;
			cout << "Maxim = " << tree.Maxim(tree.root)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 6:
		{
			CLEAR;
			cout << "Succesor = " << tree.Succesor(tree.root)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 7:
		{
			CLEAR;

			Node* node = tree.Search(3);
			cout << "Predecesor = " << tree.Predecesor(node)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 8:
		{
			CLEAR;

			cout << "0. Exit Print \n";
			cout << "1. Print Preorder \n";
			cout << "2. Print Inorder \n";
			cout << "3. Print Postorder \n";
			cout << "4. Print LevelOrder \n";

			while (1)
			{
				int opt;
				cout << "option = ";
				cin >> opt;

				switch (opt)
				{

				case 0:
					return 0;

				case 1:
					tree.PrintTree(1);
					break;

				case 2:
					tree.PrintTree(2);
					break;

				case 3:
					tree.PrintTree(3);
					break;

				case 4:
					tree.PrintTree(4);
					break;

				default:
					break;
				}
			}
			PAUSE;
			CLEAR;
		}break;

		case 9:
		{
			CLEAR;

			tree.Empty();

			PAUSE;
			CLEAR;
		}break;

		}
	}

	system("pause");

	return 0;
}