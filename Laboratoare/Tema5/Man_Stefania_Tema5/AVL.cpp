#include <iostream>
#include <fstream>
#include <queue>
#define CLEAR system("cls")
#define PAUSE system("pause")

using namespace std;

struct Node
{
	int info;
	int balance;
	int h;

	Node* parent;
	Node* left;
	Node* right;

	Node(int info = 0, int balance = 0, int height = 0)
	{
		this->info = info;
		this->balance = balance;
		this->h = height;

		parent = left = right = nullptr;
	}
};

struct ARN
{
	Node* root;

	ARN()
	{
		root = nullptr;
	}

	Node* Maxim(Node* x)
	{
		Node* y = x;

		while (y->right != nullptr)
			y = y->right;

		return y;
	}

	Node* Minim(Node* x)
	{
		Node* y = x;

		while (y->left != nullptr)
			y = y->left;

		return y;
	}

	Node* Succesor(Node* x)
	{
		Node* y = nullptr;

		if (x->right != nullptr)
		{
			y = Minim(x->right);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->right)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Predecesor(Node* x)
	{
		Node* y = nullptr;

		if (x->left != nullptr)
		{
			y = Maxim(x->left);

			return y;
		}

		y = x->parent;

		while (y != nullptr && x == y->left)
		{
			x = y;
			y = y->parent;
		}

		return y;
	}

	Node* Search(int k)
	{
		Node* x = root;

		while (x != nullptr && x->info != k)
		{
			if (k < x->info)
				x = x->left;
			else
				x = x->right;
		}

		return x;
	}

	void Rot_St(Node* x)
	{
		Node* y = x->right;
		x->right = y->left;

		if (y->left != nullptr)
			y->left->parent = x;

		y->parent = x->parent;

		if (x->parent == nullptr)
			root = y;
		else if (x == x->parent->left)
			x->parent->left = y;
		else
			x->parent->right = y;

		y->left = x;
		x->parent = y;
	}

	void Rot_Dr(Node* x)
	{
		Node* y = x->left;
		x->left = y->right;

		if (y->right != nullptr)
			y->right->parent = x;

		y->parent = x->parent;

		if (x->parent == nullptr)
			root = y;
		else if (x == x->parent->right)
			x->parent->right = y;
		else
			x->parent->left = y;

		y->right = x;
		x->parent = y;
	}

	int Factor(Node *x)
	{
		int hs = 0, hd = 0;

		if (x->left)
			hs = x->left->h;

		if (x->right) 
			hd = x->right->h;

		return hd - hs;
	}
	void Rebalansare(Node *x)
	{

		if (Factor(x) == -2)
		{
			Node* y = x->left;

			if (Factor(y) == 1)
				Rot_St(y);

			Rot_Dr(x);

		}
		else
		{
			Node* y = x->right;

			if (Factor(y) == -1)
				Rot_Dr(y);

			Rot_St(x);
		}
	}

	void Insert(Node* z, int info)
	{
		z->info = info;

		Node* y = nullptr;
		Node* x = root;

		while (x != nullptr)
		{
			y = x;

			if (z->info < x->info)
				x = x->left;
			else
				x = x->right;
		}

		z->parent = y;

		if (y == nullptr)
			root = z;
		else
		{
			if (z->info < y->info)
				y->left = z;
			else
				y->right = z;
		}

		z->parent = y;
		x = z->parent;

		while (x)
		{
			int hs = 0, hd = 0;

			if (x->left) 
				hs = x->left->h;

			if (x->right) 
				hd = x->right->h;

			x->h = (hs < hd ? hd : hs) + 1; 

			x = x->parent;
		}

		x = z->parent;

		while (x)
		{
			if (Factor(x) == -2 || Factor(x) == 2)
				Rebalansare(x);

			x = x->parent;

		}
		z->left = nullptr;
		z->right = nullptr;

		Rebalansare(z);
	}

	void Transplant(Node* u, Node* v)
	{
		if (u->parent == nullptr)
			root = v;
		else if (u == u->parent->left)
			u->parent->left = v;
		else
			u->parent->right = v;

		v->parent = u->parent;
	}

	void Delete(Node* z)
	{

		Node *x = z->parent;

		if (z->left == nullptr)
			Transplant(z, z->right);
		else if (z->right == nullptr)
				Transplant(z, z->left);
		else
		{
			Node *y = Succesor(z);
			x = y->parent;

			if (z->right != y)
			{
				Transplant(y, y->right);
				y->right = z->right;
				y->right->parent = y;
			}

			Transplant(z, y);
			y->left = z->left;
			y->left->parent = y;
		}

		while (x)
		{
			int hs = 0, hd = 0;

			if (x->left)
				hs = x->left->h;

			if (x->right)
				hd = x->right->h;

			x->h = (hs < hd ? hd : hs) + 1;

			x = x->parent;
		}

		x = z->parent;

		while (x)
		{
			if (Factor(x) == -2 || Factor(x) == 2)
				Rebalansare(x);

			x = x->parent;

		}

		delete z;
	}

	void Construct(int* keys, int size)
	{
		for (int i = 0; i < size; i++)
		{
			Node* x = new Node;
			x->info = keys[i];
			Insert(x, x->info);
		}

		cout << "Constructed!\n";
	}

	void Empty()
	{
		if (root == nullptr)
			return;

		while (root != nullptr)
			delete root;

		cout << "Empty!\n";
	}


	void PrintPreorder(Node* node)
	{
		if (node == nullptr)
			return;

		cout << node->info << " ";
		PrintPreorder(node->left);
		PrintPreorder(node->right);
	}

	void PrintInorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintInorder(node->left);
		cout << node->info << " ";
		PrintInorder(node->right);
	}

	void PrintPostorder(Node* node)
	{
		if (node == nullptr)
			return;

		PrintPostorder(node->left);
		PrintPostorder(node->right);
		cout << node->info << " ";
	}

	void PrintLevelOrder()
	{
		if (root == nullptr)
			return;

		queue<Node*> q;
		q.push(root);

		while (q.empty() == false)
		{
			Node* node = q.front();
			q.pop();

			cout << node->info << " ";

			if (node->left != nullptr)
				q.push(node->left);

			if (node->right != nullptr)
				q.push(node->right);
		}
	}

	void PrintTree(int opt)
	{
		switch (opt)
		{

		case 0:
			return;

		case 1:
			cout << "Preorder: ";
			PrintPreorder(root);
			cout << '\n';
			break;

		case 2:
			cout << "Inorder: ";
			PrintInorder(root);
			cout << '\n';
			break;

		case 3:
			cout << "Postorder: ";
			PrintPostorder(root);
			cout << '\n';
			break;

		case 4:
			cout << "LevelOrder: ";
			PrintLevelOrder();
			cout << '\n';
			break;

		default:
			break;
		}
	}
};

int main()
{
	ARN tree;

	int keys[5] = { 2, 4, 3, 1, 5 };

	tree.Construct(keys, 5);

	while (1)
	{
		//Show menu
		std::cout << "0. EXIT \n";
		std::cout << "1. INSERT \n";
		std::cout << "2. SEARCH \n";
		std::cout << "3. DELETE \n";
		std::cout << "4. MINIM \n";
		std::cout << "5. MAXIM \n";
		std::cout << "6. SUCCESOR \n";
		std::cout << "7. PREDECESOR \n";
		std::cout << "8. PRINT_TREE \n";
		std::cout << "9. EMPTY \n";

		//Get choice
		int choice;
		std::cout << "choice:";
		std::cin >> choice;

		//Options
		switch (choice)
		{
		case 0:
			return 0;

		case 1:
		{
			CLEAR;

			Node* node = new Node;

			int key;
			cout << "key = ";
			cin >> key;

			tree.Insert(node, key);

			cout << "AVL after insertion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 2:
		{	CLEAR;

		int key;
		std::cout << "key = ";
		std::cin >> key;

		Node* node = tree.Search(key);

		if (node != nullptr)
			cout << "Nodul " << node->info << " a fost gasit!\n";
		else
			cout << "Nodul " << node->info << " nu a fost gasit!\n";

		PAUSE;
		CLEAR;
		}break;

		case 3:
		{
			CLEAR;

			int key;
			std::cout << "key = ";
			std::cin >> key;

			Node* node = tree.Search(key);

			if (node != nullptr)
				tree.Delete(node);

			cout << "AVL after deletion: \n";
			tree.PrintTree(4);

			PAUSE;
			CLEAR;
		}break;

		case 4:
		{
			CLEAR;
			Node* node = tree.Minim(tree.root);

			cout << "Minim = " << node->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 5:
		{
			CLEAR;
			cout << "Maxim = " << tree.Maxim(tree.root)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 6:
		{
			CLEAR;
			cout << "Succesor = " << tree.Succesor(tree.root)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 7:
		{
			CLEAR;

			Node* node = tree.Search(3);
			cout << "Predecesor = " << tree.Predecesor(node)->info << '\n';

			PAUSE;
			CLEAR;
		}break;

		case 8:
		{
			CLEAR;

			cout << "0. Exit Print \n";
			cout << "1. Print Preorder \n";
			cout << "2. Print Inorder \n";
			cout << "3. Print Postorder \n";
			cout << "4. Print LevelOrder \n";

			while (1)
			{
				int opt;
				cout << "option = ";
				cin >> opt;

				switch (opt)
				{

				case 0:
					return 0;

				case 1:
					tree.PrintTree(1);
					break;

				case 2:
					tree.PrintTree(2);
					break;

				case 3:
					tree.PrintTree(3);
					break;

				case 4:
					tree.PrintTree(4);
					break;

				default:
					break;
				}
			}
			PAUSE;
			CLEAR;
		}break;

		case 9:
		{
			CLEAR;

			tree.Empty();

			PAUSE;
			CLEAR;
		}break;

		}
	}

	system("pause");

	return 0;
}