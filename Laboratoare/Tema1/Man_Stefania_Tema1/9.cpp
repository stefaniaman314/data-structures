//Tema1 Pb9 Subpct a)

#include <iostream>

void readMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
			std::cin >> matrix[line][column];
	}
}

void printMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
		{
			std::cout << matrix[line][column] << " ";
		}
		std::cout << '\n';
	}
}

void printArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";

	std::cout << '\n';
}

int main()
{
	int **matrix;
	int n;

	std::cout << "Enter size:";
	std::cin >> n;

	matrix = new int*[n];

	for (int i = 0; i < n; i++)
		matrix[i] = new int[n];

	readMatrix(matrix, n);

	std::cout << "Matrix: \n";
	printMatrix(matrix, n);

	int *arr;
	int size = (n * (n + 1)) / 2;

	arr = new int[size];

	size = 0;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < i + 1; j++)
			arr[size++] = matrix[i][j];

	std::cout << "Array: ";
	printArray(arr, size);
	
	delete arr;

	for (int i = 0; i < n; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}