#include <iostream>

void readMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
			std::cin >> matrix[line][column];

	}
}

void printMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
		{
			std::cout << matrix[line][column] << " ";
		}
		std::cout << '\n';
	}
}
void printSquareSum(int **matrix, int n)
{
	int top = 0;
	int bottom = n - 1;
	int left = 0;
	int right = n - 1;

	int sum = 0;

	while (left < right && top < bottom)
	{
		sum = 0;

		for (int i = left; i <= right; i++)
			sum += matrix[top][i];

		top++;

		for (int i = top; i <= bottom; i++)
			sum += matrix[i][right];

		right--;


		for (int i = right; i >= left; i--)
			sum += matrix[bottom][i];

		bottom--;

		for (int i = bottom; i >= top; i--)
			sum += matrix[i][left];

		left++;

		std::cout << sum << " ";
	}
	
	if (n % 2 != 0)
		std::cout << matrix[n / 2][n / 2];
	
}

int main()
{
	int **matrix;
	int n;

	std::cout << "Enter size:";
	std::cin >> n;

	matrix = new int*[n];

	for (int i = 0; i < n; i++)
		matrix[i] = new int[n];


	readMatrix(matrix, n);

	std::cout << "Matrix: \n";
	printMatrix(matrix, n);

	std::cout << "Sumele: ";

	printSquareSum(matrix, n);

	std::cout << '\n';

	for (int i = 0; i < n; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}