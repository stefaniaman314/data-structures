#include <iostream>

void readArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
}

void printArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";

	std::cout << '\n';
}

void verify(int *arr, int n)
{
	int asc = 0;
	int desc = 0;

	for(int i = 0; i < n; i++)
		for (int j = i + 1; j < n - 1; j++)
		{
			if (arr[i] < arr[j])
				asc = 1;
			else
				if (arr[i] > arr[j])
					desc = 1;
		}

	if (asc == 1 && desc == 1)
		std::cout << "Unsorted array.\n";
	else 
		if (asc == 1)
		std::cout << "Sorted array (ascending).\n";
	else 
		if (desc == 1)
		std::cout << "Sorted array (descending).\n";
}

int main()
{
	int n;
	std::cout << "Enter n: ";
	std::cin >> n;

	int *arr;
	arr = new int[n];

	readArray(arr, n);

	std::cout << "Array: ";
	printArray(arr, n);
	
	verify(arr, n);

	delete arr;

	system("pause");

	return 0;
}