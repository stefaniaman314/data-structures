#include <iostream>

void readMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
			std::cin >> matrix[line][column];
		
	}
}

void printMatrix(int **matrix, int n)
{
	for (int line = 0; line < n; ++line)
	{
		for (int column = 0; column < n; ++column)
		{
			std::cout << matrix[line][column] << " ";
		}
		std::cout << '\n';
	}
}

int main()
{
	int n;
	std::cout << "number of lines and columns = ";
	std::cin >> n;

	int **matrix;
	matrix = new int*[n];

	for (int i = 0; i < n; i++)
		matrix[i] = new int[n];

	readMatrix(matrix, n);

	std::cout << "Matrix: \n";
	printMatrix(matrix, n);

	std::cout << "Elevii care se agreeaza reciproc: ";

	for (int i = 0; i <= n - 2; i++)
		for (int j = i + 1; j <= n - 1; j++)
			if (matrix[i][j] == 1 && matrix[j][i] == 1)
				std::cout << "(" << i + 1 << "," << j + 1 << ") , ";

	std::cout << '\n';

	std::cout << "Elevii care nu agreeaza pe nimeni: ";

	int ok = 1;
	int student;

	for (int i = 0; i < n; i++)
	{
		ok = 1;

		for (int j = 0; j < n; j++)
		{

			if (matrix[i][j] == 1)
			{
				ok--;
				student = i + 1;
			}

		}
		if (ok >= 0)
			std::cout << student << " ";
	}


	std::cout << '\n';

	std::cout << "Elevii care nu sunt agreeati de nimeni: ";

	for (int j = 0; j < n; j++)
	{
		ok = 1;

		for (int i = 0; i < n; i++)
		{

			if (matrix[i][j] == 1)
			{
				ok--;
				student = j + 1;
			}

		}
		if (ok >= 0)
			std::cout << student << " ";
	}


	std::cout << '\n';

	for (int i = 0; i < n; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}