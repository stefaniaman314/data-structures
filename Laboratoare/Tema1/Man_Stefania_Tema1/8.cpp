#include <iostream>

void readArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
}

void printArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";

	std::cout << '\n';
}

void printMatrix(int **matrix, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			std::cout << matrix[i][j];

		std::cout << '\n';
	}

	std::cout << '\n';
}

int main()
{
	int n;
	std::cout << "Introduceti n = "; 
	std::cin >> n;

	int *arr;
	arr = new int[n];

	std::cout << "Introduceti elementele: " << '\n';
	readArray(arr, n);

	std::cout << "Vectorul: ";
	printArray(arr, n);

	std::cout << '\n';

	int **matrix;
	matrix = new int *[n];

	for (int i = 0; i < n; i++)
		matrix[i] = new int[n];

	int index1 = 0;
	int index2 = 0;

	for (int i = 0; i < n; i++)
	{
		index1 = index2;

		for (int j = 0; j < n; j++)
		{
			if (index1 == n)
				index1 = 0;

			matrix[i][j] = arr[index1++];
		}

		index2 = i + 1;
	}

	std::cout << "Matricea: \n";
	printMatrix(matrix, n);

	delete arr;

	for (int i = 0; i < n; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}
