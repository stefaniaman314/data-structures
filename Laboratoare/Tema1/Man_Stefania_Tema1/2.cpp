#include <iostream>

void readArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
}

void printArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";

	std::cout << '\n';
}

int maxArray(int *arr, int n)
{
	int maxim = arr[0];
	int index = 0;

	for(int i = 1; i < n; i++)
		if (maxim < arr[i])
		{
			maxim = arr[i];
			index = i;
		}

	return index;	
}

int main()
{
	int n;
	std::cout << "Enter n: ";
	std::cin >> n;

	int *arr;
	arr = new int[n];

	readArray(arr, n);

	std::cout << "Array: ";
	printArray(arr, n);

	int index = maxArray(arr, n);

	int *frecv;
	frecv = new int[arr[index] + 1];

	for (int i = 0; i < arr[index]; i++)
		frecv[i] = 0;

	for (int i = 0; i < n; i++)
		frecv[arr[i]]++;

	int count = maxArray(frecv, arr[index] + 1);

	std::cout << count << " apare de " << frecv[count] << " ori.\n";

	delete frecv;
	delete arr;

	system("pause");

	return 0;
}
