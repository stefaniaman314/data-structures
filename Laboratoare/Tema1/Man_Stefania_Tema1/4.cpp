#include <iostream>

void readMatrix(int **matrix, const int &nLines, const int &nColumns)
{
	for (int line = 0; line < nLines; ++line)
		for (int column = 0; column < nColumns; ++column)
			std::cin >> matrix[line][column];
}

void writeMatrix(int **matrix, const int &nLines, const int &nColumns)
{
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
			std::cout << matrix[line][column] << " ";
		
		std::cout << '\n';
	}
	std::cout << '\n';
}

bool deleteLine(int **matrix, int &nLines, const int &nColumns, const int &lineToDelete)
{
	if (lineToDelete > nLines)
	{
		std::cout << "Could not delete line " << lineToDelete << ". Index out of bounds\n";

		return false;
	}

	for (int line = lineToDelete; line < nLines - 1; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
		{
			matrix[line][column] = matrix[line + 1][column];
		}
	}

	nLines--;

	return true;
}

bool deleteColumn(int **matrix, const int &nLines, int &nColumns, const int &columnToDelete)
{
	if (columnToDelete > nColumns)
	{
		std::cout << "Could not delete column " << columnToDelete << ". Index out of bounds\n";
		
		return false;
	}
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = columnToDelete; column < nColumns - 1; ++column)
		{
			matrix[line][column] = matrix[line][column + 1];
		}
	}
	
	nColumns--;
	
	return true;
}

int main()
{
	int nLines, nColumns;

	std::cout << "number of lines = ";
	std::cin >> nLines;

	std::cout << "number of columns = ";
	std::cin >> nColumns;

	int **matrix;

	matrix = new int*[nLines];

	for (int i = 0; i < nLines; i++)
		matrix[i] = new int[nColumns];

	readMatrix(matrix, nLines, nColumns);

	std::cout << "Matrix: \n";
	writeMatrix(matrix, nLines, nColumns);

	int line, column;

	std::cout << "delete line: ";
	std::cin >> line;

	std::cout << "delete column: ";
	std::cin >> column;

	deleteLine(matrix, nLines, nColumns, line);
	deleteColumn(matrix, nLines, nColumns, column);

	std::cout << "Matrix after deletion: \n";
	writeMatrix(matrix, nLines, nColumns);

	for (int i = 0; i < nLines; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}