#include <iostream>

void formSpiral(int **matrix, int n)
{
	int top = 0;
	int bottom = n - 1;
	int left = 0;
	int right = n - 1;

	int number = 1;

	while (left < right && top < bottom)
	{
		for (int i = left; i <= right; i++)
			matrix[top][i] = number++;

		top++;

		for (int i = top; i <= bottom; i++)
			matrix[i][right] = number++;

		right--;

		for (int i = right; i >= left; i--)
			matrix[bottom][i] = number++;

		bottom--;

		for (int i = bottom; i >= top; i--)
			matrix[i][left] = number++;

		left++;
	}
}

void formCenterSpiral(int **matrix, int n)
{
	int top = 0;
	int bottom = n - 1;
	int left = 0;
	int right = n - 1;

	int number = n * n;

	while (left < right && top < bottom)
	{
		for (int i = left; i <= right; i++)
			matrix[top][i] = number--;

		top++;


		for (int i = top; i <= bottom; i++)
			matrix[i][right] = number--;

		right--;


		for (int i = right; i >= left; i--)
			matrix[bottom][i] = number--;

		bottom--;

		for (int i = bottom; i >= top; i--)
			matrix[i][left] = number--;

		left++;
	}

	matrix[n/2][n/2] = 1;
}

void printMatrix(int **matrix, int n)
{
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
			std::cout << matrix[i][j] << " ";
		
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

int main()
{
	int **matrix;
	int n;

	std::cout << "Enter size:";
	std::cin >> n;

	matrix = new int*[n];

	for (int i = 0; i < n; i++)
		matrix[i] = new int[n];

	if (n % 2 == 0)
	{
		formSpiral(matrix, n);

		std::cout << "Spiral matrix: \n";

		printMatrix(matrix, n);

	}
	else
	{
		formCenterSpiral(matrix, n);

		std::cout << "Center spiral matrix: \n";

		printMatrix(matrix, n);

	}

	for (int i = 0; i < n; i++)
		delete matrix[i];

	delete matrix;

	system("pause");

	return 0;
}