#include <iostream>

void readArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cin >> arr[i];
}

void printArray(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << " ";

	std::cout << '\n';
}

void minElem(int *arr, int n, int &minim, int &count)
{
	minim = arr[0];
	count = 1;

	for (int i = 1; i < n; i++)
	{
		if (minim == arr[i])
			count++;

		else 
			if (minim > arr[i])
			{
				minim = arr[i];
				count = 1;
			}
	}
}

int main()
{
	int n;
	std::cout << "Enter n: ";
	std::cin >> n;

	int *arr;
	arr = new int[n];

	readArray(arr, n);

	std::cout << "Array: ";
	printArray(arr, n);

	int minim, count;

	minElem(arr, n, minim, count);

	std::cout << "Minimul: " << minim << " apare de " << count << " ori.\n";

	delete arr;

	system("pause");

	return 0;
}