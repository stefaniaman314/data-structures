#include <iostream>
using namespace std;

template<typename K, typename V>
struct HashNode
{
	V value;
	K key;

	HashNode(K key, V value)
	{
		this->value = value;
		this->key = key;
	}
};

template<typename K, typename V>
class HashMap
{
	HashNode<K, V> **arr;
	int capacity;
	int size;

public:
	HashMap()
	{
		capacity = 20;
		size = 0;
		arr = new HashNode<K, V>*[capacity];

		for (int i = 0; i < capacity; i++)
			arr[i] = NULL;
	}
	
	int hashCode(K key)
	{
		return key % capacity;
	}

	void insertNode(K key, V value)
	{
		HashNode<K, V> *temp = new HashNode<K, V>(key, value);

		int hashIndex = hashCode(key);

		while (arr[hashIndex] != NULL && arr[hashIndex]->key != key
			&& arr[hashIndex]->key != -1)
		{
			hashIndex++;
			hashIndex %= capacity;
		}

		if (arr[hashIndex] == NULL || arr[hashIndex]->key == -1)
			size++;
		arr[hashIndex] = temp;
	}

	void display()
	{
		for (int i = 0; i<capacity; i++)
		{
			if (arr[i] != NULL && arr[i]->key != -1)
				cout << "key = " << arr[i]->key
				<< " value = " << arr[i]->value << '\n';
		}
	}
};

int main()
{
	HashMap<int, int> *h = new HashMap<int, int>;

	int n = 200;

	for (int i = 0; i < n; i++)
		h->insertNode(rand(), rand());

	h->display();

	system("pause");

	return 0;
}