#include<iostream>
#include <fstream>

#define MAX_N 20

using namespace std;

void readGraph(int graph[][MAX_N], int &n)
{
	ifstream f("graph.in");
	f >> n;

	int s, d;

	while (f >> s >> d)
		graph[s][d] = 1;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (graph[i][j] != 1)
				graph[i][j] = 0;

	f.close();
}

void printAdjMatrix(int graph[][MAX_N], int &n)
{
	cout << "Matricea de adiacenta: \n";

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
			cout << graph[i][j] << " ";
		cout << '\n';
	}
}

bool isConex(int graph[][MAX_N], int n)
{
	int edges = 0;
	
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (graph[i][j] == 1)
				edges++;
	
	if (edges == n - 1)
		return true;

	return false;
}

bool isCycle(int graph[][MAX_N], int n, int node)
{
	bool visited[20];
	visited[node] = true;

	for (int i = 0; i < n; i++)
		if (graph[node][i] == 1)
		{
			graph[i][node] = 0;

			if (!visited[node])
				isCycle(graph, n, i);
			else
				return true;
		}

	return false;
}

int main()
{
	int graph[MAX_N][MAX_N], n;

	readGraph(graph, n);
	printAdjMatrix(graph, n);

	if (isConex(graph, n) == true && isCycle(graph, n, 1) == false)
		cout << "Este arbore! \n";
	else
		cout << "Nu este arbore! \n";
	
	system("pause");

	return 0;
}