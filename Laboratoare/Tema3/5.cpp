#include<iostream>
using namespace std;

struct Node
{
	//Constructor
	Node(int data = 0)
	{
		this->data = data;
		child = NULL;
		next = NULL;
	}

	int data;
	Node *child;   //legatura catre cel mai din stanga fiu
	Node *next;    //legatura catre primul frate din dreapta al nodului
	
};

//Creaza un nou nod
Node* newNode(int data)
{
	Node *newNode = new Node;
	newNode->next = newNode->child = NULL;
	newNode->data = data;

	return newNode;
}

//Adauga un frate
Node* addSibling(Node *n, int data)
{
	if (n == NULL)
		return NULL;

	while (n->next)
		n = n->next;

	return (n->next = newNode(data));
}

//Adauga un fiu
Node* addChild(Node * n, int data)
{
	if (n == NULL)
		return NULL;

	if (n->child)
		return addSibling(n->child, data);
	else
		return (n->child = newNode(data));
}

//Printeaza parcurgerea in latime
void printLevelOrder(Node * root)
{
	if (root == NULL)
		return;

	while (root)
	{
		cout << " " << root->data;

		if (root->child)
			printLevelOrder(root->child);

		root = root->next;
	}
}

int main()
{
	//Crearea arborelui
	Node *root = new Node(8);
	Node* n1 = addChild(root, 9);
	Node *n2 = addChild(root, 5);
	Node *n3 = addChild(n2, 6);
	Node *n4 = addChild(n2, 3);
	Node *n5 = addChild(root, 0);
	Node *n6 = addChild(root, 7);
	Node *n7 = addChild(n6, 1);
	Node *n8 = addChild(n6, 2);
	Node *n9 = addChild(n6, 11);
	
	cout << "Parcurgerea in latime: ";
	printLevelOrder(root);

	cout << '\n';

	system("pause");

	return 0;
}