#include<iostream>
#include<fstream>
#include<queue>
#include <stack>

using namespace std;

struct Node
{
	Node(int data = 0, int nChildren = 0)
	{
		this->data = data;
		this->nChildren = nChildren;
		nodes = new Node*[nChildren];
	}
	
	int data;
	int nChildren;
	Node **nodes;
};

class Tree
{
public:
	//Citeste arborele din fisier
	void readTree(std::ifstream &file);

	//Printeaza arborele parcurs in latime
	void printLevelOrder();

	//Printeaza arborele parcurs in adancime
	void printPreorder();

private:
	Node *root;
};

void Tree::readTree(std::ifstream &file)
{
	int key, n;
	file >> key >> n;

	root = new Node(key, n);

	queue<Node *> q;
	q.push(root);

	while (!q.empty())
	{
		Node *parent = q.front();
		q.pop();

		for (int i = 0; i < parent->nChildren; i++)
		{
			file >> key;
			file >> n;

			parent->nodes[i] = new Node(key, n);

			if (n)
				q.push(parent->nodes[i]);
		}
	}
}

void Tree::printLevelOrder()
{
	if (root == nullptr)
		return;

	queue<Node *> q;
	q.push(root);

	while (!q.empty())
	{
		Node *current = q.front();
		q.pop();

		cout << current->data << ", ";

		for (int i = 0; i < current->nChildren; i++)
			q.push(current->nodes[i]);
	}

	cout << '\n';
}

void Tree::printPreorder()
{
	if (root == nullptr)
		return;

	stack<Node *> s;
	s.push(root);

	while (!s.empty())
	{
		Node *current = s.top();
		s.pop();

		cout << current->data << ", ";

		for (int i = current->nChildren - 1; i > -1; i--)
			s.push(current->nodes[i]);
	}
	cout << '\n';
}

int main()
{
	ifstream file("tree.txt");
	
	Tree T;
	T.readTree(file);

	cout << "Parcurgere in latime: ";
	T.printLevelOrder();

	cout << "Parcurgere in adancime: ";
	T.printPreorder();

	system("pause");

	return 0;
}