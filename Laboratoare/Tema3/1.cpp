#include<iostream>
#include <list>

using namespace std;

//Graf orientat reprezentat prin liste de adiacenta
class Graph
{
public:
	//Constructor
	Graph(int n);

	//Adauga o muchie la graf
	void addEdge(int v, int w);

	//Verifica daca exista drum intre doua noduri date
	bool isPath(int s, int d);

	
private:
	int n;    	       //nr de noduri
	list<int> *arr;    //pointer catre un vector de liste inlantuite
};

Graph::Graph(int n)
{
	this->n = n;
	arr = new list<int>[n];
}

void Graph::addEdge(int v, int w)
{
	arr[v].push_back(w);
}


bool Graph::isPath(int s, int d)
{
	if (s == d)
		return true;

	bool *visited = new bool[n];
	for (int i = 0; i < n; i++)
		visited[i] = false;

	list<int> queue;

	visited[s] = true;
	queue.push_back(s);

	// Iterator folosit pentru a accesa toate nodurile adiacente
	list<int>::iterator it;

	while (!queue.empty())
	{
		s = queue.front();
		queue.pop_front();

		for (it = arr[s].begin(); it != arr[s].end(); ++it)
		{
			if (*it == d)
				return true;

			if (!visited[*it])
			{
				visited[*it] = true;
				queue.push_back(*it);
			}
		}
	}
	return false;
}
int main()
{
	//Crearea grafului
	Graph g(6);
	g.addEdge(1, 2);
	g.addEdge(1, 6);
	g.addEdge(2, 3);
	g.addEdge(3, 4);
	g.addEdge(4, 2);
	g.addEdge(4, 5);
	g.addEdge(5, 3);
	g.addEdge(5, 6);

	int s = 1, d = 5;
	
	if (g.isPath(s, d))
		cout << "Exista drum intre " << s << " si " << d << '\n';
	else
		cout << "Nu exista drum intre " << s << " si " << d << '\n';

	s = 5, d = 2;

	if (g.isPath(s, d))
		cout << "Exista drum intre " << s << " si " << d << '\n';
	else
		cout << "Nu exista drum intre " << s << " si " << d << '\n';

	s = 4, d = 1;

	if (g.isPath(s, d))
		cout << "Exista drum intre " << s << " si " << d << '\n';
	else
		cout << "Nu exista drum intre " << s << " si " << d << '\n';

	system("pause");

	return 0;
}
