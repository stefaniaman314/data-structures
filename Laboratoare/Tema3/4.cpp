#include <iostream>

using namespace std;

#define N 4		  //nr de noduri
#define INF 99999 //pt noduri care nu sunt conectate

void printPath(int pred[][N]); 

void floydWarshall(int graph[][N])
{
	int pred[N][N], dist[N][N];

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			dist[i][j] = graph[i][j];

			if (graph[i][j] == INF)
				pred[i][j] = INF;
			else
				pred[i][j] = i;
		}

	for (int k = 0; k < N; k++)
	{
		for (int i = 0; i < N; i++)
			
			for (int j = 0; j < N; j++)
				
				if (dist[i][k] + dist[k][j] < dist[i][j])
				{
					dist[i][j] = dist[i][k] + dist[k][j];
					pred[i][j] = pred[k][j];
				}
	}

	printPath(pred);
}

void minPath(int pred[][N], int nod_a, int nod_b)
{
	int arr[50];
	int k = N;
	arr[k] = nod_b;

	while (arr[k] != nod_a)
	{
		arr[k - 1] = pred[nod_a][arr[k]];
		k--;
	}

	cout << "Cale_minima (" << nod_a + 1 << ", " << nod_b + 1 << ") = ";

	for (int i = k; i <= N; i++)
		cout << arr[i] + 1 << " ";

	cout << '\n';
}

void printPath(int pred[][N])
{
	for (int i = 0; i < N; i++)
		for (int j = i + 1; j < N; j++)
			minPath(pred, i, j);
}


int main()
{
	int graph[N][N] = { { 0, 4, 3,INF},
						{ 4, 0, 8, 1 },
						{ 3, 8, 0, 3 },
						{ INF,1, 3, 0} 
	};


	floydWarshall(graph);

	system("pause");

	return 0;
}