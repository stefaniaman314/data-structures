#include <iostream>
#include <fstream>
#include "Priority_Queue.h"

Priority_Queue::Priority_Queue(int dim)
{
	size = dim;
	data = new int[size];
}

Priority_Queue::~Priority_Queue()
{
	if (data != nullptr)
	{
		delete[] data;

		size = 0;
	}
}

void Priority_Queue::insert(int val)
{
	data[size] = 0;
	size++;

	increase_key(size - 1, val);
}

void Priority_Queue::extract_max()
{
	if (size < 1)
		return;

	else
	{
		data[0] = data[size - 1];
		size--;

		max_heapfy(0);
	}
}

int Priority_Queue::max_element()
{
	return data[0];
}

void Priority_Queue::increase_key(int i, int val)
{
	if (val < data[i])
		std::cout << "Valoare prea mica!\n";

	else
	{
		data[i] = val;

		int p = (i - 1) / 2;

		while (i > 0 && data[p] < val)
		{
			data[i] = data[p];
			i = p;
			p = (i - 1) / 2;
		}

		data[i] = val;
	}
}

void Priority_Queue::construct_heap()
{
	for (int i = size / 2 - 1; i >= 0; i--)
		max_heapfy(i);
}

void Priority_Queue::max_heapfy(int i)
{
	int st = 2 * i + 1;
	int dr = 2 * i + 2;
	int imax = i;

	if (st < size && data[st] > data[i])
		imax = st;

	if (dr < size && data[dr] > data[imax])
		imax = dr;

	if (imax != i)
	{
		int temp = data[imax];
		data[imax] = data[i];
		data[i] = temp;

		max_heapfy(imax);
	}
}

void Priority_Queue::read_heap()
{
	std::ifstream fin("in.txt");

	fin >> size;

	data = new int[size];

	for (int i = 0; i < size; i++)
		fin >> data[i];
}

void Priority_Queue::print_heap()
{
	for (int i = 0; i < size; i++)
		std::cout << data[i] << " ";

	std::cout << '\n';
}
