#include <iostream>
#include "Priority_Queue.h"

using namespace std;

int main()
{
	//Show menu
	cout << "== MENU == \n";

	cout << '\n';

	cout << "1. INSERT \n";
	cout << "2. EXTRACT_MAX \n";
	cout << "3. MAX_ELEMENT \n";
	cout << "4. INCREASE_KEY \n";
	cout << "5. PRINT_K_HEAP \n";

	cout << '\n';

	cout << "0. EXIT \n";

	cout << '\n';

	Priority_Queue queue(0, 0);

	queue.read_heap();
	queue.construct_k_heap();


	while (1)
	{

		//Get choice
		int choice;
		cout << "choice: ";
		cin >> choice;

		switch (choice)
		{
		case 0:
			return 0;

		case 1:
		{
			int value;
			cout << "Enter value: ";
			cin >> value;

			queue.insert(value);

			cout << "Max priority-queue after insertion: ";
			queue.print_heap();

		}break;

		case 2:
		{
			queue.extract_max();

			cout << "Max priority-queue after extraction: ";
			queue.print_heap();

		}break;

		case 3:
		{
			cout << "Max element: " << queue.max_element() << "\n";

		}break;

		case 4:
		{
			int index;
			cout << "Enter index: ";
			cin >> index;

			int value;
			cout << "Enter value: ";
			cin >> value;

			queue.increase_key(index, value);

			cout << "Max priority-queue after increase: ";
			queue.print_heap();

		}break;

		case 5:
		{
			cout << "Max priority-queue: ";
			queue.print_heap();

		}break;

		}
	}

	system("pause");

	return 0;
}