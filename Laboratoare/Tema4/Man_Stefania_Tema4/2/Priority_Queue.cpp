#include <iostream>
#include <fstream>
#include "Priority_Queue.h"

Priority_Queue::Priority_Queue(int dim, int copii)
{
	size = dim;
	data = new int[size];
	k = copii;
}

Priority_Queue::~Priority_Queue()
{
	if (data != nullptr)
	{
		delete[] data;

		size = 0;
		k = 0;
	}
}

void Priority_Queue::insert(int val)
{
	data[size] = 0;
	size++;

	increase_key(size - 1, val);
}

void Priority_Queue::extract_max()
{
	if (size < 1)
		return;

	else
	{
		data[0] = data[size - 1];
		size--;

		max_heapfy(0);
	}
}

int Priority_Queue::max_element()
{
	return data[0];
}

void Priority_Queue::increase_key(int i, int val)
{
	if (val < data[i])
		std::cout << "Valoare prea mica!\n";

	else
	{
		data[i] = val;

		int p = (i - 1) / k;

		while (i > 0 && data[p] < val)
		{
			data[i] = data[p];
			i = p;
			p = (i - 1) / k;
		}

		data[i] = val;
	}
}

void Priority_Queue::construct_k_heap()
{
	for (int i = (size - 1) / k; i >= 0; i--)
		max_heapfy(i);
}

void Priority_Queue::max_heapfy(int index)
{
	//array - retine indicii tuturor copiilor nodului 'index'
	int *children;
	children = new int[k + 1];

	while (1)
	{
		for (int i = 0; i < k; i++)
		{
			if (k * index + i < size)
				children[i] = k * index + i;

			else
				//daca nodul este frunza
				children[i] = -1;
		}

		int max_child = -1, max_child_index;

		//afla maximul dintre copiii nodului
		for (int i = 0; i < k; i++)
		{
			if (children[i] != -1 && data[children[i]] > max_child)
			{
				max_child_index = children[i];
				max_child = data[children[i]];
			}
		}

		//frunza
		if (max_child == -1)
			break;

		//interschimba daca cheia max_child > cheia nodului
		if (data[index] < data[max_child_index])
		{
			int temp = data[index];
			data[index] = data[max_child_index];
			data[max_child_index] = temp;
		}

		index = max_child_index;
	}
}

void Priority_Queue::read_heap()
{
	std::ifstream fin("in.txt");

	fin >> size >> k;

	data = new int[size];

	for (int i = 0; i < size; i++)
		fin >> data[i];
}

void Priority_Queue::print_heap()
{
	for (int i = 0; i < size; i++)
		std::cout << data[i] << " ";

	std::cout << '\n';
}
