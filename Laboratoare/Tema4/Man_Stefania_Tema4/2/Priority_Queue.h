#pragma once

class Priority_Queue
{
public:
	//Constructor
	Priority_Queue(int dim = 0, int copii = 0);

	//Destructor
	~Priority_Queue();

	//Insereaza un nou nod in coada
	void insert(int val);

	//Extrage elem de prioritate max 
	void extract_max();

	//Returneaza elem de prioritate max
	int max_element();

	//Creste prioritatea unui nod
	void increase_key(int i, int val);

	//Construieste heap-ul max din vectorul data
	void construct_k_heap();

	//Coboara o cheie pe pozitia potrivita in heap
	void max_heapfy(int index);

	//Citeste un vector de numere din fisier
	void read_heap();

	//Printeaza pe consola vectorul de numere
	void print_heap();

private:
	int *data;
	int size;
	int k;
};
