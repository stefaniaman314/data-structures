#include <iostream>

struct Nod
{
	int info;

	Nod *next;
};

struct Coada
{
	Nod *prim, *ultim;

	Coada()
	{
		prim = ultim = NULL;
	}

	bool isEmpty()
	{
		if (prim == NULL)
			return true;

		return false;
	}

	void push(int info)
	{
		Nod *elem = new Nod;

		elem->info = info;
		elem->next = NULL;

		if (isEmpty() == true)
			prim = ultim = elem;

		else
		{
			ultim->next = elem;
			ultim = elem;
		}

	}

	void pop()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		else
		{
			Nod *copy = NULL;

			copy = prim;

			prim = prim->next;

			delete copy;
		}
	}

	int top()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		return prim->info;
	}
};

int main()
{
	Coada c;

	int n;
	std::cout << "n= ";
	std::cin >> n;

	int info;

	for (int i = 0; i < n; i++)
	{
		std::cin >> info;

		c.push(info);
	}

	 
	std::cout << "Elementele: \n";

	for (int i = 0; i < n; i++)
	{
		std::cout << c.top() << " ";

		c.pop();
	}

	std::cout << '\n';

	system("pause");

	return 0;
}
