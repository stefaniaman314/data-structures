#include <iostream>

struct Nod
{
	int info;
	Nod *next;
};

struct Lista
{
	Nod *cap;

	//Constructor
	Lista()
	{
		cap = NULL;
	}

	void insert(int info)
	{
		Nod *elem = new Nod;

		elem->info = info;
		elem->next = cap;
		cap = elem;
	}

	Nod* search(int info)
	{
		Nod *elem;

		elem = cap;

		while (elem != NULL && elem->info != info)
			elem = elem->next;

		return elem;
	}

	void deleteElem(Nod* elem)
	{
		Nod* prev;

		if (elem == cap)
			cap = cap->next;

		else
		{
			prev = cap;

			while (prev->next != elem)
				prev = prev->next;

			prev->next = elem->next;
		}

		delete elem;

	}

	bool isEmpty()
	{
		if (cap == NULL)
			return true;

		return false;
	}

	void inverse()
	{
		Nod* precedent = NULL;
		Nod* curent = cap;
		Nod* urmator;

		while (curent != NULL)
		{
			urmator = curent->next;
			curent->next = precedent;
			precedent = curent;
			curent = urmator;
		}
		cap = precedent;
	}

	void insertEnd(int k)
	{
		Nod* elem = new Nod;

		elem->info = k;
		elem->next = NULL;

		if (IsEmpty() == true)
			cap = elem;

		else
		{
			Nod *nod = cap;

			while (nod->next != NULL)
				nod = nod->next;

			nod->next = elem;
		}
	}

	void insertAfterNode(Nod* nod, int info)
	{
		Nod* elem = new Nod;

		elem->info = info;
		elem->next = nod->next;
		nod->next = elem;

	}

	void insertAfter(int k, int m)
	{

		if (search(m) == NULL)
			insertEnd(k);
		else
		{
			Nod* nod = search(m);

			insertAfterNode(nod, k);
		}
	}

	
	void insertNPoz(int n, int k, int p, int size)
	{
		if (size >= p)
		{
			int index = 0;

			Nod *nod;
			nod = cap;

			//gasesc nodul dupa care trebuie sa fac inserarea (de pe pozitia p)
			while (nod != NULL && index != p)
			{
				nod = nod->next;
				index++;
			}

			for (int i = 0; i < n; i++)
				insertAfterNode(nod, k);
		}

		else
			if(size < p)
				for (int i = 0; i < n; i++)
					insertEnd(k);
	}

	void printList()
	{
		Nod *nod;
		nod = cap;

		while (nod != NULL)
		{
			std::cout << nod->info << " ";

			nod = nod->next;
		}

		std::cout << '\n';
	}

};

int main()
{
	Lista obj;

	int size;
	std::cout << "Size: ";
	std::cin >> size;

	int info;

	std::cout << "Enter elements: ";
	for (int i = 0; i < size; i++)
	{
		std::cin >> info;

		obj.insert(info);
	}

	//Show menu
	std::cout << "0. EXIT \n";
	std::cout << "1. PRINT_LIST \n";
	std::cout << "2. INSERT \n";
	std::cout << "3. SEARCH \n";
	std::cout << "4. DELETE \n";
	std::cout << "5. IS_EMPTY \n";
	std::cout << "6. INVERSE \n";
	std::cout << "7. INSERT_AFTER \n";
	std::cout << "8. INSERT_N_POZ \n";

	while (1)
	{
		//Get choice
		int choice;
		std::cout << "choice:";
		std::cin >> choice;

		//Options
		switch (choice)
		{
			case 0: 
				return 0;

			case 1:
			{	
				std::cout << "Lista :";
				obj.printList();

			}break;

			case 2:
			{
				int elem;
				std::cout << "element: ";
				std::cin >> elem;

				obj.insert(elem);

				std::cout << "Lista dupa insertie:";
				obj.printList();

			}break;

			case 3:
			{
				int elem;
				std::cout << "element: ";
				std::cin >> elem;

				if (obj.search(elem) == NULL)
					std::cout << "Elemenul nu exista! \n";
				else
					std::cout << "Elemenul " << obj.search(elem)->info << " a fost gasit. \n";

			}break;

			case 4:
			{
				int elem;
				std::cout << "element: ";
				std::cin >> elem;

				if(obj.search(elem) == NULL)
					std::cout << "Elemenul nu exista! \n";

				else
				{
					obj.deleteElem(obj.Search(elem));

					std::cout << "Lista dupa stergere:";
					obj.printList();
				}

			}break;

			case 5:
			{
				if (obj.isEmpty() == true)
					std::cout << "Lista goala! \n";
				else
					std::cout << "Lista nu e goala.\n";

			}break;

			case 6:
			{
				obj.inverse();

				std::cout << "Lista dupa inversiune:";
				obj.printList();

			}break;

			case 7:
			{
				int k, m;
				std::cout << "k= "; std::cin >> k;
				std::cout << "m= "; std::cin >> m;

				obj.insertAfter(k, m);

				std::cout << "Lista dupa insertie:";
				obj.printList();

			}break;

			case 8:
			{
				int n, key, pos;
				std::cout << "n= "; std::cin >> n;
				std::cout << "key= "; std::cin >> key;
				std::cout << "pos= "; std::cin >> pos;

				obj.insertNPoz(n, key, pos, size);

				std::cout << "Lista dupa insertie:";
				obj.printList();

			}break;

		}
	}

	system("pause");

	return 0;
}