#include <iostream>

struct Nod
{
	int x;
	int y;
	int pasi;

	Nod *next;
};

struct Coada
{
	Nod *prim, *ultim;

	Coada()
	{
		prim = ultim = NULL;
	}

	bool isEmpty()
	{
		if (prim == NULL)
			return true;

		return false;
	}

	void push(int x, int y, int pasi)
	{
		Nod* elem = new Nod;

		elem->x = x;
		elem->y = y;
		elem->pasi = pasi;

		elem->next = NULL;

		if (isEmpty() == true)
			prim = ultim = elem;

		else
		{
			ultim->next = elem;
			ultim = elem;
		}
	}

	void pop()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		else
		{
			Nod *copy = NULL;

			copy = prim;

			prim = prim->next;

			delete copy;
		}
	}
	

};

void readMatrix(int **matrix, int nLines, int nColumns)
{
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
			std::cin >> matrix[line][column];

	}
}

void printMatrix(int **matrix, int nLines, int nColumns)
{
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
		{
			std::cout << matrix[line][column] << " ";
		}
		std::cout << '\n';
	}
}

void drumMin(int **matrix, int nLines, int nColumns, int x0, int y0, int x1, int y1)
{
	Coada c;

	int x = x0;
	int y = y0;
	int pasi = 0;

	c.push(x, y, pasi);
	
	do
	{
		Nod *nod = prim;
		c.pop();
		pasi++;
		x = nod->x;
		y = nod->y;

		if (x + 1 > nLines - 1)
		{
			if (matrix[x + 1][y] != -1)
			{
				c.push(pasi, x + 1, y);
				matrix[x + 1][y] = pasi;
			}
		}

		if (x - 1  > nLines - 1)
		{
			if (matrix[x - 1][y] != -1)
				c.push(pasi, x - 1 , y);

			matrix[x - 1][y] = pasi;
		}
		
		if (y + 1 > nColumns - 1)
		{
			if (matrix[x][y + 1] != -1)
				c.push(pasi, x, y + 1);

			matrix[x][y + 1] = pasi;
		}

		if (y - 1 > nColumns - 1)
		{
			if (matrix[x][y - 1] != -1)
				c.push(pasi, x, y - 1);

			matrix[x][y - 1] = pasi;
		}
		
	} while (x != x1 && y != y1 && c.isEmpty() == false);

	/*std::cout << c.prim->pasi << '\n';*/
}

int main()
{
	int nLines;
	std::cout << "number of lines = ";
	std::cin >> nLines;

	int nColumns;
	std::cout << "number of columns = ";
	std::cin >> nColumns;

	int **matrix;
	matrix = new int*[nLines];

	for (int i = 0; i < nLines; i++)
		matrix[i] = new int[nColumns];

	readMatrix(matrix, nLines, nColumns);

	int x0;
	std::cout << "x0 = "; 
	std::cin >> x0;

	int y0;
	std::cout << "y0 = ";
	std::cin >> y0;

	int x1;
	std::cout << "x1 = ";
	std::cin >> x1;

	int y1;
	std::cout << "y1 = ";
	std::cin >> y1;

	//std::cout << "Drumul minim: " << (matrix, nLines, nColumns, x0, y0, x1, y1) << " pasi.\n";

	(matrix, nLines, nColumns, x0, y0, x1, y1);

	printMatrix(matrix, nLines, nColumns);

	for (int i = 0; i < nLines; i++)
		delete matrix[i];

	delete matrix;


	system("pause");

	return 0;
	
}