#include <iostream>

struct Nod
{
	int info;
	
	Nod *next;
};

struct Stiva
{
	Nod *varf;

	Stiva()
	{
		varf = NULL;
	}

	void push(int info)
	{
		Nod *elem = new Nod;

		elem->info = info;

		elem->next = varf;

		varf = elem;
	}

	bool isEmpty()
	{
		if (varf == NULL)
			return true;

		return false;
	}

	void pop()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		else
		{
			Nod *elem ;

			elem = varf;

			varf = varf->next;

			delete elem;
		}
			
	}

	int top()
	{
		if (isEmpty() == true)
			std::cout << "Empty\n";

		return varf->info;
	}

};

int main()
{
	Stiva s;

	int n;
	std::cin >> n;

	int info;
	
	for (int i = 0; i < n; i++)
	{
		std::cin >> info;

		s.push(info);
	}


	std::cout << "Elementele: \n";

	while (s.isEmpty() == false)
	{
		std::cout << s.top() << " ";

		s.pop();
	}

	std::cout << '\n';

	system("pause");
	
	return 0;
}