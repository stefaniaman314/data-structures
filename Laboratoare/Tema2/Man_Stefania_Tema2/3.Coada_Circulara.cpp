#include <iostream>

struct CoadaCirculara
{
	int *data;
	int size_max;
	int begin, end;

	CoadaCirculara(int dim = 0)
	{
		data = new int[dim];
		size_max = dim;
		begin = 0;
		end = 0;
	}

	~CoadaCirculara()
	{
		if (data != NULL)
			delete[] data;

		size_max = 0;
		begin = 0;
		end = 0;
	}

	void alloc(int dim)
	{
		if(data != NULL)
			delete[] data;

		data = new int[dim];
		size_max = dim;
		begin = 0;
		end = 0;
	}

	bool isFull()
	{
		if (begin == end + 1)
			return true;

		return false;
	}

	void push(int info)
	{
		if (isFull() == true)
			std::cout << "Full!\n";
		else
		{
			if (size_max == end)
				end = 0;

			data[end++] = info;
		}
			
	}

	bool isEmpty()
	{
		if (begin == end)
			return true;

		return false;
	}

	void pop()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		else
		{
			if (begin == size_max)
				begin = 0;

			begin++;
		}

	}

	void print()
	{
		for (int i = begin; i < end; i++)
		{
			if (begin == size_max)
				begin = 0;

			std::cout << data[i] << " ";
		}
	}

	void print2(bool da)
	{
		for (int i = begin; i < end; i++)
		{
			std::cout << data[i] << " ";

			pop();
		}
	}
};

int main()
{
	int n;
	std::cin >> n;

	CoadaCirculara elem(n);

	int info;

	//citire
	for (int i = 0; i < n; i++)
	{
		std::cin >> info;

		elem.push(info);
	}

	std::cout << "Elementele: ";

	elem.print2(true);
	std::cout << '\n';
	
	system("pause");

	return 0;

}