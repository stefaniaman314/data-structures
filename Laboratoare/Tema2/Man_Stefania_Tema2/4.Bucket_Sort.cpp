#include <iostream>

struct Nod
{
	float info;
	Nod *prev;
	Nod *next;
};

struct ListaDubla
{
	Nod *cap;

	ListaDubla()
	{
		cap == NULL;
	}
	
	void printList()
	{
		Nod *nod;
		nod = cap;

		while (nod != NULL)
		{
			std::cout << nod->info << " ";

			nod = nod->next;
		}

		std::cout << '\n';
	}

	void insert(float info)
	{
		Nod* elem = new Nod;

		elem->info = info;
		elem->next = cap;
		elem->prev = NULL;

		if (cap != NULL)
			cap->prev = elem;

		cap = elem;
	}

	Nod* search(int info)
	{
		Nod *elem;
		elem = cap;

		while (elem != NULL && elem->info != info)
			elem = elem->next;

		return elem;
	}

	void deleteElem(Nod *elem)
	{
		if (elem->prev != NULL)
			elem->prev->next = elem->next;

		else
			cap = elem->next;

		if (elem->next != NULL)
			elem->next->prev = elem->prev;
	}

	//insereaza o cheie intr-o lista sortata, pe poz potrivita
	void insertSort(float info)
	{
		Nod *elem = new Nod;

		elem->info = info;

		//inserare la capul listei
		if (cap == NULL || cap->info >= elem->info)
			insert(info);
		else
		{
			Nod *curent = cap;

			while (curent->next != NULL && curent->next->info < elem->info)
				curent = curent->next;

			elem->next = curent->next;

			if (curent->next != NULL)
				elem->next->prev = elem;

			elem->next = curent->next;
			curent->next->prev = elem;
			curent->next = elem;
			elem->prev = curent;
		}
	}

	bool isEmpty()
	{
		if (cap == NULL)
			return true;

		return false;
	}

	Nod* head()
	{
		return cap;
	}
};

int main()
{
	int size;
	std::cout << "size:";
	std::cin >> size;

	float *arr;
	arr = new float[size];
	
	ListaDubla *buckets;
	buckets = new ListaDubla[size];
	
	//citire vector de numere 
	for (int i = 0; i < size; i++)
		std::cin >> arr[i];

	system("pause");

	return 0;
}