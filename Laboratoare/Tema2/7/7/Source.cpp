#include <iostream>
struct coord
{
	int x;
	int y;
};
struct Nod
{
	int x;
	int y;
	int pasi;

	Nod *next;
};

struct Coada
{
	Nod *prim, *ultim;

	Coada()
	{
		prim = ultim = NULL;
	}

	bool isEmpty()
	{
		if (prim == NULL)
			return true;

		return false;
	}

	void push(int x, int y, int pasi)
	{
		Nod* elem = new Nod;

		elem->x = x;
		elem->y = y;
		elem->pasi = pasi;

		elem->next = NULL;

		if (isEmpty() == true)
			prim = ultim = elem;

		else
		{
			ultim->next = elem;
			ultim = elem;
		}
	}

	void pop()
	{
		if (isEmpty() == true)
			std::cout << "Empty!\n";

		else
		{
			Nod *copy = NULL;

			copy = prim;

			prim = prim->next;

			delete copy;
		}
	}

	
};

void readMatrix(int matrix[4][4], int nLines, int nColumns)
{
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
			std::cin >> matrix[line][column];

	}
}

void printMatrix(int matrix[4][4], int nLines, int nColumns)
{
	for (int line = 0; line < nLines; ++line)
	{
		for (int column = 0; column < nColumns; ++column)
		{
			std::cout << matrix[line][column] << " ";
		}
		std::cout << '\n';
	}
}

void drumMin(int matrix[4][4], int nLines, int nColumns, int x0, int y0, int x1, int y1)
{
	Coada c;

	int x = x0;
	int y = y0;
	int pasi = 1;

	c.push(x, y, pasi);
	matrix[x][y] = 1;

	do
	{
		//Nod *nod = c.top();

		
		pasi++;
		x =c.prim->x;
		y = c.prim->y;
		c.pop();
		if (x + 1 <= nLines - 1)
		{
			if (matrix[x + 1][y] == 0)
			{
				c.push(pasi, x + 1, y);
				matrix[x + 1][y] = matrix[x][y]+1;
			}
		}

		if (x - 1  <= nLines - 1)
		{
			if (matrix[x - 1][y] == 0)
			{
				c.push(pasi, x - 1, y);

				matrix[x - 1][y] = matrix[x][y] + 1;
			}
		}

		if (y + 1 <= nColumns - 1)
		{
			if (matrix[x][y + 1] == 0)
			{
				c.push(pasi, x, y + 1);

				matrix[x][y + 1] = matrix[x][y] + 1;
			}
		}

		if (y - 1 <= nColumns - 1)
		{
			if (matrix[x][y - 1] == 0)
			{
				c.push(pasi, x, y - 1);

				matrix[x][y - 1] = matrix[x][y] + 1;
			}
		}

	} while (c.isEmpty() == false &&(x != x1 || y != y1));

	/*std::cout << c.prim->pasi << '\n';*/
}

int main()
{
	int nLines=4;
	//std::cout << "number of lines = ";
	//std::cin >> nLines;

	int nColumns=4;
	//std::cout << "number of columns = ";
	//std::cin >> nColumns;
		
		/*int **matrix;
	matrix = new int*[nLines];

	for (int i = 0; i < nLines; i++)
		matrix[i] = new int[nColumns];

	readMatrix(matrix, nLines, nColumns);*/
	int matrix[4][4] = {  {-1, 0,  0, -1},
							{-1, 0, 0, -1},
							{-1, -1, 0, -1},
							{-1, -1, 0, 0}};

	int x0= 3;
	//std::cout << "x0 = ";
	//std::cin >> x0;

	int y0 = 3;
	/*std::cout << "y0 = ";
	std::cin >> y0;*/

	int x1=0;
	/*std::cout << "x1 = ";
	std::cin >> x1;*/

	int y1=1;
	/*std::cout << "y1 = ";
	std::cin >> y1;
*/
	//std::cout << "Drumul minim: " << (matrix, nLines, nColumns, x0, y0, x1, y1) << " pasi.\n";

	drumMin(matrix, nLines, nColumns, x0, y0, x1, y1);

	std::cout << "Matricea: \n";
	printMatrix(matrix, nLines, nColumns);

	/*for (int i = 0; i < nLines; i++)
		delete matrix[i];

	delete matrix;
*/

	system("pause");

	return 0;

}